# ServiceManager

## Config usage

For each module in "src/" you can build a directory "config" with a config file "module.config.php"
(src/MyModule/config/module.config.php).

All files will be merged in one big config. The config can be accessed by getFromConfig() in service-manager.
Simply use the keys as param like:

```php
$exampleConfig = [
    'my_config' => [
        'module1' => [
            'foo' => 'bar',
        ],
    ],
];

$serviceManager->getFromConfig(['my_config', 'module1', 'foo']) // -> bar
```

## Registering factories

Each module witch uses factories need a specific key in the "module.config.php". You can simply register a mapping of classes
and their Factories. For classes witch doesnt need a factory, register a InvokableFactory, witch will return the class without
parameter.

```php
'service_manager' => [
    'factories' => [
        MyClassWithDependencies::class => MyFactory::class,             // implements FactoryInterface
        MyClassWithoutDependencies::class => InvokableFactory::class,   // just returns the object
    ],
],
```

## FactoryInterface

A factory for a class with dependencies has to implement FactoryInterface. Dependencies can simply accessed by the container.

```php
public function __invoke(ServiceManagerInterface $container, ExternalServiceManager $externals)
    {
        return new MyClass(
            $container->get(MyDependency::class)
        );
    }
```

## External Service-Manager or Container

The constructor of the ServiceManager has an optional parameter for injection of an external service-manager or container.
For this purpose use the ExternalServiceManager interface. You can access the external SM or container with the $externals
variable in the invoke-method of the factory.