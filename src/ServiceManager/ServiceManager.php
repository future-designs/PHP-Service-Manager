<?php
/**
 * @copyright Copyright (c) 2018 TraSo GmbH (www.traso.de)
 * @author j.lohse
 * @since 04.06.18
 */

namespace Traso\XIBE\ServiceManager;

/**
 * Class ServiceManager
 * @package Traso\XIBE\ServiceManager
 */
class ServiceManager implements ServiceManagerInterface
{
    const CODE_DIR = 'src/';
    const CONFIG_DIR = '/config/';
    const CONFIG_PATTERN = 'module.config.php';

    /** @var ExternalServiceManager */
    protected $externalSM;

    /** @var array */
    protected $services;

    /** @var array */
    protected $config;

    /** @var array */
    protected $classMap;

    public function __construct(ExternalServiceManager $externalSM = null)
    {
        $this->externalSM = $externalSM;
        $this->config = $this->getConfig();
        $this->classMap = $this->config['service_manager']['factories'];
    }

    /**
     * @param string $class
     * @return mixed
     * @throws \Exception
     */
    public function get(string $class)
    {
        if (!empty($this->services)) {
            foreach ($this->services as $service) {
                if ($service instanceof $class) {
                    return $service;
                }
            }
        }

        return $this->buildObject($class);
    }

    /**
     * @param array $args
     * @return array|mixed
     * @throws \Exception
     */
    public function getFromConfig(array $args)
    {
        $argCount = count($args);
        $config = $this->config;
        $count = 0;

        foreach ($args as $arg) {
            if (isset($config[$arg])) {
                $config = $config[$arg];
            } else {
                throw new \Exception('Key ' . $arg . ' not found in config!');
            }

            if ($count === $argCount) {
                break;
            }

            $count++;
        }

        return $config;
    }

    /**
     * @param string $class
     * @return mixed
     * @throws \Exception
     */
    private function buildObject(string $class)
    {
        $serviceFactory = null;
        $serviceClass = null;
        foreach ($this->classMap as $service => $factory) {
            if ($class === $service) {
                $serviceClass = $class;
                $serviceFactory = $factory;
                break;
            }
        }

        if (null === $serviceFactory) {
            throw new \Exception('Factory for class ' . $class . ' not found!');
        }

        $serviceFactory = new $serviceFactory();

        if ($serviceFactory instanceof InvokableFactory) {
            $requestedObject = $serviceFactory($serviceClass);
            $this->services[] = $requestedObject;
            return $requestedObject;
        }

        if ($serviceFactory instanceof FactoryInterface) {
            $requestedObject = $serviceFactory($this, $this->externalSM);
            $this->services[] = $requestedObject;
            return $requestedObject;
        } else {
            throw new \Exception('Factory ' . $serviceFactory . ' should implement ' . FactoryInterface::class);
        }
    }

    /**
     * @return array
     */
    private function getConfig(): array
    {
        $config = [];
        $directories = scandir(self::CODE_DIR);

        foreach ($directories as $directory) {
            if ($directory === '.' || $directory === '..') {
                continue;
            }

            if (is_dir(self::CODE_DIR . $directory . self::CONFIG_DIR)) {
                if (is_file(self::CODE_DIR . $directory . self::CONFIG_DIR . self::CONFIG_PATTERN)) {
                    $moduleConfig = require_once self::CODE_DIR . $directory . self::CONFIG_DIR . self::CONFIG_PATTERN;
                    $config = array_merge_recursive($config, $moduleConfig);
                }
            }
        }

        return $config;
    }
}