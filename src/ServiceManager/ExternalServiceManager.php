<?php
/**
 * @copyright Copyright (c) 2018 TraSo GmbH (www.traso.de)
 * @author j.lohse
 * @since 04.06.18
 */

namespace Traso\XIBE\ServiceManager;

/**
 * Interface ExternalServiceManager
 * @package Traso\XIBE\ServiceManager
 */
interface ExternalServiceManager
{
    public function get($class);
}